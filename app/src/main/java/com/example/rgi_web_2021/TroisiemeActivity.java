package com.example.rgi_web_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TroisiemeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troisieme);
    }

    public void onClickQuitterTroisiemeActivite(View view) {

        Intent intentResultThird = new Intent();

        intentResultThird.putExtra("cle_boolean", true);
        intentResultThird.putExtra("cle_string", "Belle chaine de retour");

        setResult(RESULT_OK, intentResultThird);

        finish();
    }
}