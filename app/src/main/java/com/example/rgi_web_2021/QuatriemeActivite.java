package com.example.rgi_web_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class QuatriemeActivite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quatrieme_activite);
    }

    public void onClickQuitterQuatriemeActivite(View view) {
        Intent intentResultFourth = new Intent();

        intentResultFourth.putExtra("cle_erreur", "Il y a eu un probleme zebi");

        setResult(RESULT_CANCELED, intentResultFourth);

        finish();
    }
}