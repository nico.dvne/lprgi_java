package com.example.rgi_web_2021;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rgi_web_2021.decouverteRecyclerView.RecyclerViewActivite;

public class MainActivity extends AppCompatActivity {

    private final static int THIRD_ACTIVITY_REQUEST_CODE = 43712;
    private final static int FOURTH_ACTIVITY_REQUEST_CODE = 85456;

    private int compteur;
    private TextView monTxtView;
    private Button decrementer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compteur = 0;

        monTxtView = findViewById(R.id.txt_increment_value);
        decrementer = findViewById(R.id.btn_decrement);

        decrementer.setOnClickListener(
                v->{
                    compteur -- ;
                    MAJTextView();
                }
        );

        MAJTextView();
    }

    public void onClickBtnIncrementer(View view) {
        String message = "bravo t'as bien clique";
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        compteur++;
        MAJTextView();
    }

    private void MAJTextView(){
        String contenu = "Compteur = "+ this.compteur;

        monTxtView.setText(contenu);
    }

    public void onClickLancerActivite(View view) {
        Intent monIntent = new Intent(this, SecondeActivite.class);

        Bundle monBundle = new Bundle();
        monBundle.putInt("cle_du_compteur", compteur);
        monBundle.putDouble("cle_du_double", 3.14);

        monIntent.putExtras(monBundle);

        startActivity(monIntent);
    }

    public void onClickLancerTroisiemeActivite(View view) {
        Intent monIntent = new Intent(this, TroisiemeActivity.class);

        Bundle monBundle = new Bundle();
        monBundle.putDouble("cle_du_double", 6.28);

        monIntent.putExtras(monBundle);

        //startActivity(monIntent);
        startActivityForResult(monIntent, THIRD_ACTIVITY_REQUEST_CODE);
    }

    public void onClickLancerQuatriemeActivite(View view) {
        Intent monIntent = new Intent(this, TroisiemeActivity.class);

        Bundle monBundle = new Bundle();
        monBundle.putDouble("cle_du_double", 666.666);

        monIntent.putExtras(monBundle);

        //startActivity(monIntent);
        startActivityForResult(monIntent, FOURTH_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case THIRD_ACTIVITY_REQUEST_CODE: {

                String message = "";

                if (RESULT_OK == resultCode) {
                    message = data.getBooleanExtra("cle_boolean", false) + " " +
                            data.getStringArrayExtra("cle_string");
                }
                else {

                }

            }; break;

            case FOURTH_ACTIVITY_REQUEST_CODE: {

            }; break;
        }
    }

    public void onClickDecouverteRecyclerView(View view) {
        startActivity(new Intent(this, RecyclerViewActivite.class));
    }
}