package com.example.rgi_web_2021.decouverteRecyclerView;

public class Utilisateur {

    private String nom;
    private String prenom;

    private int poids;

    public Utilisateur(String nom, String prenom, int poids) {
        this.nom = nom;
        this.prenom = prenom;
        this.poids = poids;
    }

    public Utilisateur() {
        this("npd", "ppg", 0);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", poids=" + poids +
                '}';
    }
}
