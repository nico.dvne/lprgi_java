package com.example.rgi_web_2021.decouverteRecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rgi_web_2021.R;

import java.util.List;

/**
 * Adapteur : Lien entre le modele ( ici une liste ) et le holder
 */

public class UtilisateurAdapter extends RecyclerView.Adapter<UtilisateurHolder> {

    private List<Utilisateur> lesUtilisateurs;

    private EcouteurRecyclerView lEcouteur;

    public UtilisateurAdapter(List<Utilisateur> lesUtilisateurs, EcouteurRecyclerView lEcouteur) {
        this.lesUtilisateurs = lesUtilisateurs;
        this.lEcouteur = lEcouteur;
    }

    @NonNull
    @Override
    public UtilisateurHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View maVue = layoutInflater.inflate(R.layout.layout_ligne, parent, false);

        return new UtilisateurHolder(maVue, lEcouteur);
    }

    @Override
    public void onBindViewHolder(@NonNull UtilisateurHolder holder, int position) {
        holder.visualiserUtilisateur(lesUtilisateurs.get(position));
    }

    @Override
    public int getItemCount() {
        return lesUtilisateurs.size();
    }
}
