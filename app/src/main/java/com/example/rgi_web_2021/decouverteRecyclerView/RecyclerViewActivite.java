package com.example.rgi_web_2021.decouverteRecyclerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.example.rgi_web_2021.R;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RecyclerViewActivite extends AppCompatActivity {

    private List<Utilisateur> lesUtilisateurs;

    private TextView monTextView;
    private RecyclerView monRecyclerView;

    private UtilisateurAdapter utilisateurAdapter;

    private EcouteurRecyclerView monEcouteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_view_activite);

        initLesUtilisateurs();

        monEcouteur = new EcouteurRecyclerView() {
            @Override
            public void ilYaEuUnClickLong(int position) {
                String message = "Il y a eu un click long " + position;
                monTextView.setText(message);
            }

            @Override
            public void ilYaEuUnClickCourt(int position) {
                String message = "Il y a eu un click court " + position;
                monTextView.setText(message);
            }
        };

        monTextView = findViewById(R.id.id_textview_recyclerview);
        monRecyclerView = findViewById(R.id.id_recyclerview);

        utilisateurAdapter = new UtilisateurAdapter(lesUtilisateurs, monEcouteur);

        monRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        monRecyclerView.setAdapter(utilisateurAdapter);
    }

    private void initLesUtilisateurs() {

        this.lesUtilisateurs = IntStream.range(0, 100)
                .mapToObj(i->new Utilisateur("Nom " + i, "Prenom" + i, 10+i))
                .collect(Collectors.toList());
    }
}