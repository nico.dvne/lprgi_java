package com.example.rgi_web_2021.decouverteRecyclerView;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rgi_web_2021.R;

import java.util.Objects;

public class UtilisateurHolder extends RecyclerView.ViewHolder {

    private TextView nomTextView;
    private TextView poidsTextView;
    private EcouteurRecyclerView lEcouteur;

    public UtilisateurHolder(@NonNull View itemView, EcouteurRecyclerView lEcouteur) {
        super(itemView);

        nomTextView = itemView.findViewById(R.id.id_nom_ligne_textview);
        poidsTextView = itemView.findViewById(R.id.id_poids_textview);
        itemView.setOnClickListener(view->{lEcouteur.ilYaEuUnClickCourt(getAdapterPosition());});
        itemView.setOnLongClickListener(view -> {
            lEcouteur.ilYaEuUnClickLong(getAdapterPosition());
            return true;
        });
    }

    public void visualiserUtilisateur(Utilisateur unUtilisateur) {

        if(Objects.nonNull(unUtilisateur)) {
            nomTextView.setText(unUtilisateur.getNom());
            poidsTextView.setText(String.valueOf(unUtilisateur.getPoids()));
        }

    }
}
