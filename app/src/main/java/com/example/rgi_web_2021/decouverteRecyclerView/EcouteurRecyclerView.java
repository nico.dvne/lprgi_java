package com.example.rgi_web_2021.decouverteRecyclerView;

public interface EcouteurRecyclerView {

    void ilYaEuUnClickLong(int position);

    void ilYaEuUnClickCourt(int position);

}
