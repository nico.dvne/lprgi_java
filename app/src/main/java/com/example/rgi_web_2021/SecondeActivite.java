package com.example.rgi_web_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Objects;

public class SecondeActivite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_seconde_activite);

        Intent monIntent = getIntent();

        Bundle leBundle = monIntent.getExtras();

        if( Objects.nonNull(leBundle) )
        {
            int entierRecu = leBundle.getInt("cle_du_compteur");
            double doubleRecu = leBundle.getDouble("cle_du_double");

            Toast
                    .makeText(this, "" + entierRecu + " / " + doubleRecu, Toast.LENGTH_LONG)
                    .show();
        }
    }

    public void onClickQuitterSecondeActivite(View view) {
        finish();
    }
}